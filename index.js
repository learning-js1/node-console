let options = "Choose an option:";
let alternatives = "1. Read Package.json\n2. Display OS info\n3. Start HTTP server\n";

const readline = require("readline");
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

//Method for reading JSON file
function readJSON(){
    const fs = require("fs");
    let jsonfile = fs.readFileSync("package.json");
    let data = JSON.parse(jsonfile);
    console.log("Reading package.json file: \n",data);
}

//Method for reading OS information
function readOS(){
    const os = require("os");

    console.log("SYSTEM MEMORY: ", os.totalmem() /1024/1024/1024, " GB");
    console.log("FREE MEMORY: ", os.freemem() /1024/1024/1024, " GB");
    console.log("CPU CORES: ", os.cpus().length);
    console.log("ARCH: ", os.arch());
    console.log("PLATFORM: ", os.platform());
    console.log("RELEASE: ", os.release());
    console.log("USER: ", os.userInfo().username);


}

//Method to start a local server and write hello world
function startServer(){
    const http = require("http");
    console.log("Starting http server...");

    const request = (req, res) => {
        res.writeHead(200);
        res.end("Hello World!");
    }


    const server = http.createServer(request);
    server.listen(3000);
    console.log("Listening on port 3000...");
}

//Writing out options and letting user chose what to do...
console.log(options);
rl.question(alternatives, chosen =>{
        switch(chosen){
    case "1":
        readJSON();
        break;
    case "2":
        readOS();
        break;      
    case "3":
        startServer();
        break;
    default:
        console.log("Option not found...")
}
rl.close();
});